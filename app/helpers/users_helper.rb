module UsersHelper

	# Returns the Gravatar (http://gravatar.com/) for the given user.

	# Should not have in the final, should have my own private image uploads for the ads
  def gravatar_for(user, options = { size: 50 })  # Setting parameters to onoly accept size
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end

end

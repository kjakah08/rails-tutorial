class Micropost < ActiveRecord::Base
	belongs_to :user
	default_scope -> { order('created_at DESC') } # In descending order
	validates :user_id, presence: true
	validates :content, presence: true, length: { maximum: 140 } # Setting length to < 141 characters, and must be present

  # Returns microposts from the users being followed by the given user.
  # Interploated SQL, bacsically what my webservice will depend on, 
  # (much more efficient to push to the database)
  def self.from_users_followed_by(user)
    followed_user_ids = "SELECT followed_id FROM relationships
                         WHERE follower_id = :user_id"
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id",
          user_id: user.id)
  end
end
